<?php

namespace Drupal\gdpr_simplenews\ConsentSubscriberResolver;

use Drupal\Core\Entity\EntityInterface;

/**
 * Interface GdprConsentSubscriberResolverInterface.
 */
interface GdprConsentSubscriberResolverInterface {

  /**
   * Gets the subscriber reference from the specified entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to use when finding the subscriber.
   *
   * @return \Drupal\simplenews\Entity\Subscriber
   *   The subscriber
   */
  public function resolve(EntityInterface $entity);

}
