<?php

namespace Drupal\gdpr_simplenews\Plugin\Gdpr\ConsentUserResolver;

use Drupal\Core\Entity\EntityInterface;
use Drupal\gdpr_consent\ConsentUserResolver\GdprConsentUserResolverInterface;

/**
 * Resolves user reference for a simplenews_subscriber entity.
 *
 * @GdprConsentUserResolver(
 *   id = "gdpr_consent_simplenews_subscriber_resolver",
 *   label = "GDPR Consent Simplenews Subscriber Resolver",
 *   entityType = "simplenews_subscriber"
 * )
 * @package Drupal\gdpr_consent\Plugin\Gdpr\ConsentUserResolver
 */
class SimplenewsSubscriberResolver implements GdprConsentUserResolverInterface {

  /**
   * {@inheritdoc}
   */
  public function resolve(EntityInterface $entity) {
    return $entity->uid->entity;
  }

}
