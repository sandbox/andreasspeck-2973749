<?php

namespace Drupal\gdpr_simplenews\Plugin\Gdpr\ConsentSubscriberResolver;

use Drupal\Core\Entity\EntityInterface;
use Drupal\gdpr_simplenews\ConsentSubscriberResolver\GdprConsentSubscriberResolverInterface;

/**
 * Resolves subscriber reference for a simplenews_subscriber entity.
 *
 * @GdprConsentSubscriberResolver(
 *   id = "gdpr_subscriber_consent_simplenews_subscriber_resolver",
 *   label = "GDPR Subscriber Consent Simplenews Subscriber Resolver",
 *   entityType = "simplenews_subscriber"
 * )
 * @package Drupal\gdpr_simplenews\Plugin\Gdpr\ConsentSubscriberResolver
 */
class SimplenewsSubscriberResolver implements GdprConsentSubscriberResolverInterface {

  /**
   * {@inheritdoc}
   */
  public function resolve(EntityInterface $entity) {
    // We need to return the subscriber entity, not the user account
    return $entity;
  }

}
