<?php

namespace Drupal\gdpr_simplenews\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\DataReferenceTargetDefinition;
use Drupal\gdpr_consent\Entity\ConsentAgreement;
use Drupal\message\Entity\Message;

/**
 * Plugin implementation of the 'gdpr_subscriber_consent' field type.
 *
 * @FieldType(
 *   id = "gdpr_subscriber_consent",
 *   label = @Translation("GDPR Subscriber Consent"),
 *   description = @Translation("Stores subscriber consent for a particular
 *   agreement"), category = @Translation("GDPR"), default_widget
 *   ="gdpr_subscriber_consent_widget", default_formatter = "gdpr_subscriber_consent_formatter"
 * )
 */
class SubscriberConsentItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return ['target_id' => '']
      + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['target_id'] = DataReferenceTargetDefinition::create('integer')
      ->setLabel('Target agreement ID')
      ->setSetting('unsigned', TRUE)
      ->setRequired(TRUE);

    $properties['target_revision_id'] = DataDefinition::create('integer')
      ->setLabel('Revision ID');

    $properties['agreed'] = DataDefinition::create('boolean')
      ->setLabel('Agreed');

    $properties['date'] = DataDefinition::create('datetime_iso8601')
      ->setLabel('Date stored');

    $properties['subscriber_id'] = DataReferenceTargetDefinition::create('integer')
      ->setLabel('Subscriber ID');

    $properties['subscriber_id_accepted'] = DataReferenceTargetDefinition::create('integer')
      ->setLabel('Subscriber ID Accepted');

    $properties['notes'] = DataReferenceTargetDefinition::create('string')
      ->setLabel('Notes');

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    $definition = $this->getFieldDefinition();

    /* @var \Drupal\gdpr_simplenews\ConsentSubscriberResolver\ConsentSubscriberResolverPluginManager $plugin_manager */
    $plugin_manager = \Drupal::service('plugin.manager.gdpr_consent_subscriber_resolver');
    $resolver = $plugin_manager->getForEntityType($definition->getTargetEntityTypeId(), $definition->getTargetBundle());
    $subscriber = $resolver->resolve($this->getEntity()); // In theory the subscriber is the entity!

    if ($subscriber != NULL) {
      $this->set('subscriber_id', $subscriber->id());
      $this->set('subscriber_id_accepted', $subscriber->id());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave($update) {
    $should_log = FALSE;

    if (!$update) {
      // Always log on a create. Is this useful for subscribers?
      $should_log = TRUE;
    }
    else {
      $field_name = $this->getFieldDefinition()->getName();
      $original_value = $this->getEntity()->original->{$field_name}->agreed;
      if ($original_value != $this->agreed) {
        $should_log = TRUE;
      }
    }

    if (substr(\Drupal::service('path.current')->getPath(),0,23) == '/newsletter/confirm/add') {
       $should_log = TRUE; // We need to log when the subscriber confirms, as otherwise we do not have the entity
    }

    if ($should_log) {
      $msg = Message::create(['template' => 'consent_subscriber_agreement']);
      $msg->set('subscriber', $this->subscriber_id);
      $msg->set('subscriber_accepted', $this->subscriber_id_accepted);
      $msg->set('agreement', ['target_id' => $this->target_id, 'target_revision_id' => $this->target_revision_id]);
      $msg->set('notes', $this->notes);
      $msg->set('agreed', $this->agreed);
      $msg->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {

    $agreement_ids = \Drupal::entityQuery('gdpr_consent_agreement')
      ->condition('status', 1)
      ->sort('title')
      ->execute();

    $agreements = ConsentAgreement::loadMultiple($agreement_ids);

    $element = [];

    $element['target_id'] = [
      '#type' => 'select',
      '#title' => 'Agreement',
      '#required' => TRUE,
      '#options' => ['' => 'Please select'] + $agreements,
      '#default_value' => $this->getSetting('target_id'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [
      'indexes' => [
        'target_id' => ['target_id'],
      ],
    ];

    $schema['columns']['target_id'] = [
      'description' => 'The ID of the target entity.',
      'type' => 'int',
      'unsigned' => TRUE,
    ];

    $schema['columns']['target_revision_id'] = [
      'description' => 'The Revision ID of the target entity.',
      'type' => 'int',
    ];

    $schema['columns']['agreed'] = [
      'description' => 'Whether the user has agreed.',
      'type' => 'int',
      'size' => 'tiny',
      'default' => 0,
    ];

    $schema['columns']['subscriber_id'] = [
      'description' => 'ID of the subscriber who has accepted.',
      'type' => 'int',
    ];

    $schema['columns']['date'] = [
      'description' => 'Time that the subscriber agreed.',
      'type' => 'varchar',
      'length' => 20,
    ];

    $schema['columns']['subscriber_id_accepted'] = [
      'description' => 'ID of the subscriber who recorded the acceptance',
      'type' => 'int',
    ];

    $schema['columns']['notes'] = [
      'description' => 'Additional notes on the acceptance',
      'type' => 'varchar',
      'length' => '255',
    ];
    return $schema;
  }

}
